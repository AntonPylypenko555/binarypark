﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryPark
{
    abstract class Car
    {
        protected double balance;
        public string PlateNumber { get; set; }


        public Car(string plateNumber, double balance = 100)
        {
            this.balance = balance;
            PlateNumber = plateNumber;
        }

        public double Balance
        {
            get
            {
                return balance;
            }
        }

        public void AddMoney(double sumOfMoney)
        {
            balance += sumOfMoney;
        }

        // Повертає гроші
        public double GiveMoney(double howMuchMoney)
        {
            balance = balance - howMuchMoney;
            if (balance >= 0)
            {
                return howMuchMoney;
            }
            return -1;
        }

        public abstract string StringTypeOfCar();
    }
}

