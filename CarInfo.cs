﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryPark
{
    class CarInfo
    {
        public Car Car { get; set; }
        public DateTime FinishTime { get; set; }
        public DateTime StartTime { get; set; }

        public CarInfo()
        {
        }

        public CarInfo(Car car, DateTime startTime)
        {
            Car = car;
            StartTime = startTime;
        }
    }
}