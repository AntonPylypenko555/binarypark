﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryPark
{
    class ParkLot
    {
        private static ParkLot parkLot = null;
        const int MAX_PLACE_COUNT = 10;
        const int PERIOD_OF_TIME = 60_000;

        public double Balance { get; private set; }

        List<LogData> logs = new List<LogData>();
        List<CarInfo> carList = new List<CarInfo>();

        protected ParkLot() { }

        public static ParkLot Initialize()
        {
            if (parkLot == null)
            {
                parkLot = new ParkLot();
            }
            return parkLot;
        }

        public void AddCar(Car car)
        {
            if (carList.Count < MAX_PLACE_COUNT)
            {
                carList.Add(new CarInfo() { Car = car, StartTime = DateTime.Now });
                logs.Add(new LogData() { Message = "Car: " + car.PlateNumber + " was added at " + DateTime.Now + "; ", Time = DateTime.Now });
                Console.WriteLine("The car is added! \n");
            }
            else
            {
                logs.Add(new LogData() { Message = "No car added at " + DateTime.Now + "; ", Time = DateTime.Now });
                throw new Exception("No place more! Sorry.");
            }
        }

        public Car GetCar(string carPlate)
        {
            Car car = null;
            if (carList.Count > 0)
            {
                CarInfo lot = carList.Where(c => c.Car.PlateNumber == carPlate).FirstOrDefault();
                if (lot != null)
                {
                    double payment = CountMoney(lot);
                    double givenMoney = lot.Car.GiveMoney(payment);
                    if (givenMoney == payment)
                    {
                        Balance += givenMoney;
                        carList.Remove(lot);
                        car = lot.Car;
                        logs.Add(new LogData() { Message = "Car: " + car.PlateNumber + " was out at " + DateTime.Now, Time = DateTime.Now });
                        Console.WriteLine("Your car is gotten!\n");
                    }
                    else
                    {
                        throw new Exception("You don't have enough money! \n");
                    }
                }
            }
            return car;
        }

        private double CountMoney(CarInfo lot)
        {
            DateTime finishTime = DateTime.Now;
            lot.FinishTime = finishTime;
            return CalculateTimeInterval(lot.StartTime, finishTime) * GetTheCoefficient(lot.Car);
        }

        //Визначає коефіцієнт для кожної машини
        public static double GetTheCoefficient(Car car)
        {
            double coefficient = 0;

            if (car is Passengercar) { coefficient = 2; }
            if (car is TruckCar) { coefficient = 5; }
            if (car is Bus) { coefficient = 3.5; }
            if (car is Motocycle) { coefficient = 1; }

            return coefficient;
        }

        //поповнює баланс для машини
        public void RechargeTheCarBalance(string plateNumber)
        {
            Console.WriteLine("Type the sum of money.");
            string strOfMoney = Console.ReadLine();
            int sumOfMoney;
            bool lineIsCorrect = Int32.TryParse(strOfMoney, out sumOfMoney);
            if (lineIsCorrect & (sumOfMoney > 0))
            {
                CarInfo lot = carList.Where(c => c.Car.PlateNumber == plateNumber).FirstOrDefault();
                lot.Car.AddMoney(sumOfMoney);
                Console.WriteLine("The sum of money is added! \n");
            }
            else
            {
                Console.WriteLine("You typed the wrong symbol, please type only numbers.\n");
            }
        }

        // Перевірка балансу.
        public void ShowTheBalanceOfCar(string plateNumber)
        {
            CarInfo lot = carList.Where(c => c.Car.PlateNumber == plateNumber).FirstOrDefault();
            Console.WriteLine("Your balance is: " + lot.Car.Balance + ". \n");
        }

        // Показує к-ть вільних та занятих лотів
        public void ShowTheAmountOfFreeAndBusyLots()
        {
            int busyLots = 0;
            int freeLots = 0;
            foreach (var item in carList)
            {
                if (item == null) freeLots++;
                else if (item != null) busyLots++;
            }

            Console.WriteLine("The amount of free lots is: " + freeLots + ";");
            Console.WriteLine("The amount of busy lots is: " + busyLots + ". \n");
        }

        // Рахує час простою машини на парковці
        public double CalculateTimeInterval(DateTime startTime, DateTime finishTime)
        {
            TimeSpan timeSpan = finishTime.Subtract(startTime);
            double d = timeSpan.TotalMinutes;
            return d;
        }

        // Показує номери усіх транспортних засобів та їх тип
        public void ShowTheWholeListOfTransports()
        {
            List<CarInfo> busyLots = new List<CarInfo>();
            foreach (var item in carList)
            {
                if (item != null)
                {
                    busyLots.Add(item);
                }
            }
            foreach (var item in busyLots)
            {
                Console.WriteLine("The type of car is: " + item.Car.StringTypeOfCar() + "and number is: " + item.Car.PlateNumber + ". \n");
            }
        }

        public void ShowLastTransactions()
        {
            DateTime time = DateTime.Now.Subtract(new TimeSpan(0, 1, 0));
            var query = logs.Where(c => c.Time > time);

            foreach (var item in query)
            {
                Console.WriteLine(item.Message);
            }
        }

        // Записує транзакції
        public void WriteLog(object obj)
        {
            DateTime time = DateTime.Now.Subtract(new TimeSpan(0, 1, 0));
            var query = logs.Where(c => c.Time > time);
            
            using (StreamWriter sw = new StreamWriter("Transaction.log", true, System.Text.Encoding.Default))
            {
                foreach (var item in query)
                {
                    sw.WriteLine(item.Message + "\n");
                }
            }
        }

        public void SetTimer()
        {
            TimerCallback tm = new TimerCallback(WriteLog);
            Timer timer = new Timer(tm, null, 0, PERIOD_OF_TIME);
        }

        public void ShowAllTransactions()
        {
            using (StreamReader sr = new StreamReader("Transaction.log", System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }
    }
}