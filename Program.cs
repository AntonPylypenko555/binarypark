﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryPark
{
    class Program
    {
        static void Main(string[] args)
        {
            ParkLot parkLot = ParkLot.Initialize();
            parkLot.SetTimer();

            bool flag = true;

            do
            {
                Console.WriteLine("   Binary studio parking lot!   ");
                Console.WriteLine("             Menu: ");
                Console.WriteLine("1. To put my car on a meter. ");
                Console.WriteLine("2. To get my car out. ");
                Console.WriteLine("3. Check your balance. ");
                Console.WriteLine("4. Recharge the balance. ");
                Console.WriteLine("5. Current balance of parklot. ");
                Console.WriteLine("6. Show transactions for the last minute. "); 
                Console.WriteLine("7. Show all transactions. ");       
                Console.WriteLine("8. The amount of free&busy places. ");
                Console.WriteLine("9. The whole list of transports. ");
                Console.WriteLine("0. Exit. ");

                string stringOfChoice = Console.ReadLine();

                // Checking of the input
                switch (stringOfChoice)
                {
                    case "1":
                        chooseTheTypeOfCar();
                        break;
                    case "2":
                        parkLot.GetCar(getTheCarNumber());
                        break;
                    case "3":
                        parkLot.ShowTheBalanceOfCar(getTheCarNumber());
                        break;
                    case "4":
                        parkLot.RechargeTheCarBalance(getTheCarNumber());
                        break;
                    case "5":
                        Console.WriteLine("The balance of the parklot: " + parkLot.Balance);
                        break;
                    case "6":
                        parkLot.ShowLastTransactions();
                        break;
                    case "7":
                        parkLot.ShowAllTransactions();
                        break;
                    case "8":
                        parkLot.ShowTheAmountOfFreeAndBusyLots();
                        break;
                    case "9":
                        parkLot.ShowTheWholeListOfTransports();
                        break;
                    case "0":
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("Please, type only 0 - 9 numbers.");
                        break;
                }                            
            } while (flag);


            void chooseTheTypeOfCar()
            {

                Console.WriteLine("\nPlease, choose your type of car.");
                Console.WriteLine("1. Passengercar; ");
                Console.WriteLine("2. Truck; ");
                Console.WriteLine("3. Bus; ");
                Console.WriteLine("4. Motocycle. ");

                string stringOfChoice = Console.ReadLine();

                switch (stringOfChoice)
                {
                    case "1":
                        parkLot.AddCar(new Passengercar(getTheCarNumber()));
                        break;
                    case "2":
                        parkLot.AddCar(new TruckCar(getTheCarNumber()));
                        break;
                    case "3":
                        parkLot.AddCar(new Bus(getTheCarNumber()));
                        break;
                    case "4":
                        parkLot.AddCar(new Motocycle(getTheCarNumber()));
                        break;
                    case "0":
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("\nPlease, type only 0 - 4 numbers.");
                        break;
                }
            }

            string getTheCarNumber()
            {

                Console.WriteLine("\nPlease, write the registration number of your car");
                return Console.ReadLine();
            }
        }
    }
}
