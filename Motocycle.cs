﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryPark
{
    class Motocycle : Car
    {
        public Motocycle(string plateNumber, double balance = 100) : base(plateNumber, balance)
        {

        }

        public override string StringTypeOfCar()
        {
            return "motocycle";
        }
    }
}